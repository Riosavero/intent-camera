package com.example.user.tugasbesarbandungtourism.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.tugasbesarbandungtourism.R;

/**
 * Created by User on 4/26/2017.
 */

public class FragmentTourism extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tourism, container,false);
    }
}
